﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KT_.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustormerId",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeId",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AccountId",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LogId",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransactionalId",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransactionalId",
                table: "Logs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CustormerId",
                table: "Accounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CustormerId",
                table: "Transactions",
                column: "CustormerId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_EmployeeId",
                table: "Transactions",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountId",
                table: "Reports",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_LogId",
                table: "Reports",
                column: "LogId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionalId",
                table: "Reports",
                column: "TransactionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_TransactionalId",
                table: "Logs",
                column: "TransactionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustormerId",
                table: "Accounts",
                column: "CustormerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Customers_CustormerId",
                table: "Accounts",
                column: "CustormerId",
                principalTable: "Customers",
                principalColumn: "CId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Transactions_TransactionalId",
                table: "Logs",
                column: "TransactionalId",
                principalTable: "Transactions",
                principalColumn: "TId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "AId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports",
                column: "LogId",
                principalTable: "Logs",
                principalColumn: "LId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TransactionalId",
                table: "Reports",
                column: "TransactionalId",
                principalTable: "Transactions",
                principalColumn: "TId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Customers_CustormerId",
                table: "Transactions",
                column: "CustormerId",
                principalTable: "Customers",
                principalColumn: "CId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Employees_EmployeeId",
                table: "Transactions",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Customers_CustormerId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Transactions_TransactionalId",
                table: "Logs");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TransactionalId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Customers_CustormerId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Employees_EmployeeId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_CustormerId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_EmployeeId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Reports_AccountId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_LogId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TransactionalId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Logs_TransactionalId",
                table: "Logs");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_CustormerId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "CustormerId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "LogId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionalId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionalId",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "CustormerId",
                table: "Accounts");
        }
    }
}
