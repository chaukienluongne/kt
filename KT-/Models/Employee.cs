﻿using System.ComponentModel.DataAnnotations;

namespace KT.Models
{
    public class Employee
    {
        [Key]
        public int EId { get; set; }
        public string FirstNameE { get; set; }
        public string LastNameE { get; set; }
        public string Address { get; set; }

        public string UserE { get; set; }
        public string Password { get; set; }
        public ICollection<Transaction> transactions { get; set; }

    }
}
