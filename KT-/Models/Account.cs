﻿using System.ComponentModel.DataAnnotations;

namespace KT.Models
{
    public class Account
    {
        [Key]
        public int AId { get; set; }

        public string AName { get; set; }

        public int CustormerId { get; set; }
        public Customer Custormer { get; set; }
        public ICollection<Report> reports { get; set; }


    }
}
