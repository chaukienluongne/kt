﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics;
namespace KT.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Report>()
                .HasOne(r => r.Transactions)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.TransactionalId);
            modelBuilder.Entity<Report>()
                .HasOne(r => r.Accounts)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.AccountId);
            modelBuilder.Entity<Report>()
                .HasOne(r => r.Logs)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.LogId);

            modelBuilder.Entity<Log>()
                .HasOne(r => r.Transactions)
                .WithMany(t => t.logs)
                .HasForeignKey(r => r.TransactionalId);

            modelBuilder.Entity<Transaction>()
                .HasOne(r => r.Employees)
                .WithMany(t => t.transactions)
                .HasForeignKey(r => r.EmployeeId);
        }

    }
}
