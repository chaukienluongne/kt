﻿using System.ComponentModel.DataAnnotations;

namespace KT.Models
{
    public class Log
    {
        [Key]
        public int LId { get; set; }

        public DateOnly Date {  get; set; }

        public TimeOnly Time { get; set; }
        public int TransactionalId { get; set; }
        public Transaction Transactions { get; set; }
        public ICollection<Report> reports { get; set; }


    }
}
