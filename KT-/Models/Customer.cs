﻿using System.ComponentModel.DataAnnotations;

namespace KT.Models
{
    public class Customer
    {
        [Key]
        public int CId { get; set; }
        public string FirstNameC { get; set; } 
        public string LastNameC { get; set; } 
        public string Address { get; set; }

        public string UserC {  get; set; }
        public string Password { get; set; }

        public ICollection<Transaction> transactions { get; set; }
        public ICollection<Account> accounts { get; set; }



    }
}
