﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace KT.Models
{
    public class Transaction
    {
        [Key]
        public int TId { get; set; }
        public string TName { get; set; }
        public int CustormerId { get; set; }
        public Customer Custormer { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employees { get; set; }
        public ICollection<Log> logs { get; set; }
        public ICollection<Report> reports { get; set; }

    }
}
