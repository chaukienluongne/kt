﻿using System.ComponentModel.DataAnnotations;

namespace KT.Models
{
    public class Report
    {
        [Key]
        public int RId { get; set; }

        public string ReportName { get; set; }
        public DateTime Rdate { get; set; }
        public int AccountId { get; set; }
        public Account Accounts { get; set; }
        public int LogId { get; set; }
        public Log Logs { get; set; }
        public int TransactionalId { get; set; }
        public Transaction Transactions { get; set; }

    }
}
